<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

function wpdocs_theme_name_scripts()
{
    //wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script('slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), true);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri() . "/resources/jquery.cookie.min.js", "", "", 1);
}
add_action('wp_enqueue_scripts', 'wpdocs_theme_name_scripts');

//Facet Title Hook
// add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
//     if ( isset( $atts['facet'] ) ) {       
//         $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
//     }
//     return $output;
// }, 10, 2 );

//add method to register event to WordPress init

add_action('init', 'register_daily_mysql_bin_log_event');

function register_daily_mysql_bin_log_event()
{
    // make sure this event is not scheduled
    if (!wp_next_scheduled('mysql_bin_log_job')) {
        // schedule an event
        wp_schedule_event(time(), 'daily', 'mysql_bin_log_job');
    }
}

add_action('mysql_bin_log_job', 'mysql_bin_log_job_function');


function mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);
}



//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter('wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new');

function wpse_100012_override_yoast_breadcrumb_trail_new($links)
{
    global $post;
    $instock = get_post_meta(@$post->ID, "in_stock");

    if (is_singular('hardwood_catalog')) {
        if ($instock[0] == "1") {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/in-stock-hardwood-products/',
                'text' => 'In Stock Hardwood Products',
            );
        } else {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/hardwood/',
                'text' => 'About Hardwood',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/hardwood/products/',
                'text' => 'Hardwood Products',
            );
        }
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('carpeting')) {
        if ($instock[0] == "1") {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/in-stock-carpet-products/',
                'text' => 'In Stock Carpet Products',
            );
        } else {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/carpet/',
                'text' => 'About Carpet',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/carpet/products/',
                'text' => 'Carpet Products',
            );
        }
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('luxury_vinyl_tile')) {
        if ($instock[0] == "1") {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/in-stock-waterproof-luxury-vinyl-products/',
                'text' => 'In Stock Waterproof Luxury Vinyl Products',
            );
        } else {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/vinyl/',
                'text' => 'Luxury Vinyl',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/vinyl/products/',
                'text' => 'Luxury Vinyl Products',
            );
        }
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('laminate_catalog')) {
        if ($instock[0] == "1") {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/in-stock-laminate-products/',
                'text' => 'In Stock Laminate Products',
            );
        } else {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/laminate/',
                'text' => 'About Laminate',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/laminate/products/',
                'text' => 'Laminate Products',
            );
        }
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('tile_catalog')) {
        if ($instock[0] == "1") {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/in-stock/in-stock-tile-products/',
                'text' => ' In Stock Tile Products',
            );
        } else {
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/tile/',
                'text' => 'About Tile',
            );
            $breadcrumb[] = array(
                'url' => get_site_url() . '/flooring/tile/products/',
                'text' => 'Tile Products',
            );
        }
        array_splice($links, 1, -1, $breadcrumb);
    } elseif (is_singular('paint-catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/paint/',
            'text' => 'Paint',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/paint/products/',
            'text' => 'Paint Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    return $links;
}
add_shortcode('customeBanner', 'stone_custom_alert_banner');
function stone_custom_alert_banner()
{

    $website_json =  json_decode(get_option('website_json'));


    foreach ($website_json->sites as $site_chat) {

        if ($site_chat->instance == 'prod') {

            if (($site_chat->banner != null || $site_chat->banner != '') &&  is_front_page()) {

                $content = '<div class="custombanneralert" style="background-color:' . $website_json->color1 . ';">' . $site_chat->banner . '</div>';

                echo $content;
            }
        }
    }
}



///Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter("wpseo_breadcrumb_links", "instockoverride_yoast_breadcrumb_pdp_page");

function instockoverride_yoast_breadcrumb_pdp_page($links)
{
    $post_type = get_post_type();

    if (is_singular('instock_carpet')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/',
            'text' => 'In-Stock Specials',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/in-stock-carpet/',
            'text' => 'Instock Carpet Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    if (is_singular('instock_lvt')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/',
            'text' => 'In-Stock Specials',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/in-stock-vinyl/',
            'text' => 'Instock Vinyl Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }
    if (is_singular('instock_hardwood')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/',
            'text' => 'In-Stock Specials',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock-specials/in-stock-hardwood/',
            'text' => 'Instock Hardwood Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }



    return $links;
}


//sigle instock post type template

// add_filter( 'single_template', 'childtheme_get_custom_post_type_template' );

function childtheme_get_custom_post_type_template($single_template)
{
    global $post;

    if ($post->post_type != 'post') {

        if ($post->post_type === 'instock_lvt' || $post->post_type === 'instock_carpet' || $post->post_type === 'instock_hardwood') {

            $single_template = get_stylesheet_directory() . '/product-listing-templates/single-' . $post->post_type . '.php';
        } else {

            $single_template = WP_PLUGIN_DIR . '/grand-child/product-listing-templates/single-' . $post->post_type . '.php';
        }
    }
    return $single_template;
}



//theme loop template changes for instock PLP
// add_filter( 'facetwp_template_html', function( $output, $class ) {

//     $prod_list = $class->query;  
//     ob_start();       

//     if(isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1){

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-instock-loop.php';

//     }else{        

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-new.php';

//     }

//     require_once $dir;    
//     return ob_get_clean();
// }, 10, 2 );

/**
 * Hide Draft Pages from the menu
 */
function filter_draft_pages_from_menu($items, $args)
{
    foreach ($items as $ix => $obj) {
        if (!is_user_logged_in() && 'draft' == get_post_status($obj->object_id)) {
            unset($items[$ix]);
        }
    }
    return $items;
}
add_filter('wp_nav_menu_objects', 'filter_draft_pages_from_menu', 10, 2);


add_filter('gform_pre_render_23', 'populate_product_color');
add_filter('gform_pre_validation_23', 'populate_product_color');
add_filter('gform_pre_submission_filter_23', 'populate_product_color');
add_filter('gform_admin_pre_render_23', 'populate_product_color');

add_filter('gform_pre_render_24', 'populate_product_color');
add_filter('gform_pre_validation_24', 'populate_product_color');
add_filter('gform_pre_submission_filter_24', 'populate_product_color');
add_filter('gform_admin_pre_render_24', 'populate_product_color');

add_filter('gform_pre_render_25', 'populate_product_color');
add_filter('gform_pre_validation_25', 'populate_product_color');
add_filter('gform_pre_submission_filter_25', 'populate_product_color');
add_filter('gform_admin_pre_render_25', 'populate_product_color');


function populate_product_color($form)
{

    foreach ($form['fields'] as &$field) {
        if ($field->id == 2) {

            $current_sku = get_the_content();
            $post_type = get_post_type();

            $brandmapping = array(
                "carpeting" => "carpet",
                "hardwood_catalog" => "hardwood",
                "laminate_catalog" => "laminate",
                "luxury_vinyl_tile" => "lvt",
                "tile_catalog" => "tile",
                "instock_carpet" => "carpet",
                "instock_hardwood" => "hardwood",
                "instock_laminate" => "laminate",
                "instock_lvt" => "lvt",
                "instock_tile" => "tile",
            );

            $current_category = $brandmapping[$post_type];

            $api_url_first = 'https://thedesignhousedfw.com/wp-json/products/v1/list?sku=' .  $current_sku . '&category=' . $current_category . '&pageName=pdp';
            $response_first = wp_remote_get($api_url_first);

            if (is_wp_error($response_first)) {
                error_log('API Request Failed: ' . $response_first->get_error_message());
                continue;
            }

            $content_first = wp_remote_retrieve_body($response_first);
            $current_product_first = json_decode($content_first, true);

            $products_group_by = $current_product_first['products_group_by'];
            if (is_array($products_group_by) && !empty($products_group_by)) {
                $collection_name = key($products_group_by);
                $first_group_products = $products_group_by[$collection_name];
                $first_product = $first_group_products[0];
            }

            $current_brand_collection_name =  $first_product['brand_collection'];
            $instock =  $first_product['in_stock'];

            $api_url = 'https://thedesignhousedfw.com/wp-json/products/v1/list?brand_collection=' . $current_brand_collection_name . '&category=' . $current_category . '&in_stock=' . $instock;
            $response = wp_remote_get($api_url);

            if (is_wp_error($response)) {
                error_log('API Request Failed: ' . $response->get_error_message());
                continue;
            }

            $body = wp_remote_retrieve_body($response);
            $data = json_decode($body, true);

            $products_group_by = $data['products_group_by'];

            $colors = [];
            $current_product_color = '';
            if ($current_brand_collection_name) {
                foreach ($products_group_by[$current_brand_collection_name] as $product) {
                    if (isset($product['name']) && !in_array($product['name'], $colors)) {
                        $colors[] = $product['name'];
                    }

                    if ($product['sku'] == $current_sku) {
                        $current_product_color = $product['name'];
                    }
                }

                $field->choices = [];
                foreach ($colors as $color) {
                    $choice = ['text' => $color, 'value' => $color];

                    if ($color === $current_product_color) {
                        $choice['isSelected'] = true;
                    }

                    $field->choices[] = $choice;
                }
            }
        }
    }

    return $form;
}
